#include "utilities/system_utils.h"

namespace srrg_g2o_slim {

double getTime() {
  struct timeval tv;
  gettimeofday(&tv, 0);
  return tv2sec(tv);
}

}
