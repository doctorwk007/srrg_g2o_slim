#pragma once

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <vector>
#include <memory>
#include <ctime>
#include <chrono>
#include <map>
#include <unordered_map>
#include <set>
#include <string>
#include <sstream>
#include <fstream>
#include <iterator>
#include <functional>
#include <cstdlib>

#include <suitesparse/cs.h>
#include <suitesparse/amd.h>
#include <suitesparse/cholmod.h>
#include <suitesparse/colamd.h>

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <Eigen/Cholesky>
#include <Eigen/StdVector>

#include "shell_colors.h"

namespace srrg_g2o_slim {

  //ia std redefinitions
  typedef double Real;

  typedef std::pair<int, int> IntPair;
  typedef std::set<int> IntSet;
  typedef std::vector<int> IntVector;
  typedef std::pair<uint64_t, uint64_t> IDPair;

  //ia basic eigen stuff
  typedef Eigen::Matrix<Real, 2, 2>     Matrix2;
  typedef Eigen::Matrix<Real, 3, 3>     Matrix3;
  typedef Eigen::Matrix<Real, 4, 4>     Matrix4;
  typedef Eigen::Matrix<Real, 6, 6>     Matrix6;
  typedef Eigen::Matrix<Real, 9, 9>     Matrix9;
  typedef Eigen::Matrix<Real, 3, 6>     Matrix3_6;
  typedef Eigen::Matrix<Real, 6, 3>     Matrix6_3;
  typedef Eigen::Matrix<Real, 5, 6>     Matrix5_6;
  typedef Eigen::Matrix<Real, 2, 3>     Matrix2_3;
  typedef Eigen::Matrix<Real, 3, 2>     Matrix3_2;
  typedef Eigen::Matrix<Real, 2, 6>     Matrix2_6;
  typedef Eigen::Matrix<Real, 6, 12>    Matrix6_12;
  typedef Eigen::Matrix<Real, 12, 6>    Matrix12_6;
  typedef Eigen::Matrix<Real, 12, 12>   Matrix12;

  typedef Eigen::Matrix<Real, 6, 1>     Vector6;
  typedef Eigen::Matrix<Real, 3, 1>     Vector3;
  typedef Eigen::Matrix<Real, 2, 1>     Vector2;
  typedef Eigen::Matrix<Real, 9, 1>     Vector9;
  typedef Eigen::Matrix<Real, 12, 1>    Vector12;

  typedef Eigen::Transform<Real,3,Eigen::Isometry>  Isometry3;
  typedef Eigen::Transform<Real,2,Eigen::Isometry>  Isometry2;
  typedef Eigen::AngleAxis<Real>                    AngleAxisReal;
  typedef Eigen::Quaternion<Real>                   QuaternionReal;

  typedef std::vector<Matrix12, Eigen::aligned_allocator<Matrix12> > Matrix12Vector;

  //ia fast hashing and comparing
  struct IntPairHasher {
    inline unsigned long operator()(const IntPair& k) const {
      std::hash<unsigned long> hash;
      return hash(((unsigned long)k.first) << 32 | k.second);
    }
  };

  struct IntPairEquals {
    inline bool operator()(const IntPair& k1, const IntPair& k2) const {
      return k1.first==k2.first && k1.second==k2.second;
    }
  };
} //ia end of the namespace
