#pragma once

#include "definitions.h"

namespace srrg_g2o_slim {

inline QuaternionReal& normalize(QuaternionReal& q){
  q.normalize();
  if (q.w()<0) {
    q.coeffs() *= -1;
  }
  return q;
}

inline Vector3 toCompactQuaternion(const Matrix3& R) {
  QuaternionReal q(R);
  normalize(q);
  // return (x,y,z) of the quaternion
  return q.coeffs().head<3>();
}

inline Matrix3 fromCompactQuaternion(const Vector3& v) {
  double w = 1-v.squaredNorm();
  if (w<0)
    return Matrix3::Identity();
  else
    w=sqrt(w);
  return QuaternionReal(w, v[0], v[1], v[2]).toRotationMatrix();
}

inline Matrix3 skew(const Vector3& p) {
  Matrix3 s;
  s <<     0,   -p.z(),    p.y(),
       p.z(),        0,   -p.x(),
      -p.y(),    p.x(),        0;
  return s;
}

inline Isometry3 v2t(const Vector6& v) {
  Isometry3 T = Isometry3::Identity();
  Matrix3 Rx, Ry, Rz;
  Rx = AngleAxisReal(v(3), Vector3::UnitX());
  Ry = AngleAxisReal(v(4), Vector3::UnitY());
  Rz = AngleAxisReal(v(5), Vector3::UnitZ());
  T.linear() = Rx * Ry * Rz;
  T.translation() = v.block<3,1>(0,0);
  return T;
}


inline Vector6 t2v(const Isometry3& t) {
  Vector6 v;
  v.head<3>()=t.translation();

  Matrix3 r = t.linear();
  Vector3 euler_angles = r.eulerAngles(0,1,2);
  v.block<3,1>(3,0) = euler_angles;

  return v;
}

//ia quaternion stuff
inline Vector6 t2vMQT(const Isometry3& t) {
  Vector6 v;
  v.block<3,1>(3,0) = toCompactQuaternion(t.linear());
  v.block<3,1>(0,0) = t.translation();
  return v;
}

inline Isometry3 v2tMQT(const Vector6& v){
  Isometry3 t;
  t = fromCompactQuaternion(v.block<3,1>(3,0));
  t.translation() = v.block<3,1>(0,0);
  return t;
}

inline Vector12 flattenIsometry(const Isometry3& T_) {
  Vector12 v;
  v.block<3,1>(0,0) = T_.matrix().block<3,1>(0,0);
  v.block<3,1>(3,0) = T_.matrix().block<3,1>(0,1);
  v.block<3,1>(6,0) = T_.matrix().block<3,1>(0,2);
  v.block<3,1>(9,0) = T_.matrix().block<3,1>(0,3);
  return v;
}

inline Isometry3 unflattenIsometry(const Vector12& vector_, const bool reconditionate_rotation_) {
  Isometry3 T = Isometry3::Identity();
  T.matrix().block<3,1>(0,0) = vector_.block<3,1>(0,0);
  T.matrix().block<3,1>(0,1) = vector_.block<3,1>(3,0);
  T.matrix().block<3,1>(0,2) = vector_.block<3,1>(6,0);
  T.matrix().block<3,1>(0,3) = vector_.block<3,1>(9,0);

  if (reconditionate_rotation_) {
    Matrix3 R = T.linear();
    Eigen::JacobiSVD<Matrix3> svd(R, Eigen::ComputeThinU | Eigen::ComputeThinV);
    Matrix3 R_enforced = svd.matrixU() * svd.matrixV().transpose();
    T.linear() = R_enforced;
  }

  return T;
}

inline Matrix3 reconditionateRotationMatrix(const Matrix3& R) {
  Eigen::JacobiSVD<Matrix3> svd(R, Eigen::ComputeThinU | Eigen::ComputeThinV);
  Matrix3 R_enforced = svd.matrixU() * svd.matrixV().transpose();
  return R_enforced;
}


} //ia end of namespace
