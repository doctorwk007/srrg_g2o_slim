namespace srrg_g2o_slim {

  #define RESET   "\033[0m"
  #define BLACK   "\033[30m"      /* Black */
  #define RED     "\033[31m"      /* Red */
  #define GREEN   "\033[32m"      /* Green */
  #define YELLOW  "\033[33m"      /* Yellow */
  #define BLUE    "\033[34m"      /* Blue */
  #define MAGENTA "\033[35m"      /* Magenta */
  #define CYAN    "\033[36m"      /* Cyan */
  #define WHITE   "\033[37m"      /* White */
  #define BOLDBLACK   "\033[1m\033[30m"      /* Bold Black */
  #define BOLDRED     "\033[1m\033[31m"      /* Bold Red */
  #define BOLDGREEN   "\033[1m\033[32m"      /* Bold Green */
  #define BOLDYELLOW  "\033[1m\033[33m"      /* Bold Yellow */
  #define BOLDBLUE    "\033[1m\033[34m"      /* Bold Blue */
  #define BOLDMAGENTA "\033[1m\033[35m"      /* Bold Magenta */
  #define BOLDCYAN    "\033[1m\033[36m"      /* Bold Cyan */
  #define BOLDWHITE   "\033[1m\033[37m"      /* Bold White */
  #define UNDERLINEDBLACK   "\033[4m\033[30m"      /* Bold Underlined Black */
  #define UNDERLINEDRED     "\033[4m\033[31m"      /* Bold Underlined Red */
  #define UNDERLINEDGREEN   "\033[4m\033[32m"      /* Bold Underlined Green */
  #define UNDERLINEDYELLOW  "\033[4m\033[33m"      /* Bold Underlined Yellow */
  #define UNDERLINEDBLUE    "\033[4m\033[34m"      /* Bold Underlined Blue */
  #define UNDERLINEDMAGENTA "\033[4m\033[35m"      /* Bold Underlined Magenta */
  #define UNDERLINEDCYAN    "\033[4m\033[36m"      /* Bold Underlined Cyan */
  #define UNDERLINEDWHITE   "\033[4m\033[37m"      /* Bold Underlined White */

} //ia end of the namespace
