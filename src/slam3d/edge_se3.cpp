#include "utilities/geometry_utils.h"
#include "edge_se3.h"

namespace srrg_g2o_slim {

EdgeSE3::EdgeSE3(VertexSE3* vertex_from_,
                 VertexSE3* vertex_to_,
                 const Isometry3& measurement_,
                 const Matrix12& information_,
                 const uint64_t& sensor_ID_) :
                      BaseEdge<Isometry3,12,Isometry3,Isometry3>(
                          vertex_from_,
                          vertex_to_,
                          measurement_,
                          information_,
                          sensor_ID_,
                          EDGE_SE3) {
}


EdgeSE3::~EdgeSE3() {}


void EdgeSE3::computeError() {
  const Isometry3& pose_i = _vertex_association.from_ptr->estimate();
  const Isometry3& pose_j = _vertex_association.to_ptr->estimate();

  //! Prediction
  Isometry3 h_x = pose_i.inverse() * pose_j;

  //! Error
  Isometry3 error_T = Isometry3::Identity();
  error_T.matrix() = h_x.matrix() - _measurement.matrix();

  _error.setZero();
  _error = flattenIsometry(error_T);
}

void EdgeSE3::computeJacobians() {
  //ia NEVER FORGET TO ZERO THE THING MANNAGGIA A DIO
  _jacobianXj.setZero();
  _jacobianXi.setZero();

  const Isometry3& pose_i = _vertex_association.from_ptr->estimate();
  const Isometry3& pose_j = _vertex_association.to_ptr->estimate();

  Matrix3 Rx0, Ry0, Rz0;
  Rx0 << 0,0,0,  0,0,-1,  0,1,0;
  Ry0 << 0,0,1,  0,0,0,   -1,0,0;
  Rz0 << 0,-1,0, 1,0,0,   0,0,0;

  Matrix3 Ri = pose_i.linear();
  Vector3 ti = pose_i.translation();

  Matrix3 Rj = pose_j.linear();
  Vector3 tj = pose_j.translation();

  Matrix3 dR_x = Ri.transpose() * Rx0 * Rj;
  Matrix3 dR_y = Ri.transpose() * Ry0 * Rj;
  Matrix3 dR_z = Ri.transpose() * Rz0 * Rj;

  Vector9 dr_x_flattened, dr_y_flattened, dr_z_flattened;
  dr_x_flattened << dR_x.col(0), dR_x.col(1), dR_x.col(2);
  dr_y_flattened << dR_y.col(0), dR_y.col(1), dR_y.col(2);
  dr_z_flattened << dR_z.col(0), dR_z.col(1), dR_z.col(2);

  //! Fill Jj
  _jacobianXj.block<9,1>(0,3) = dr_x_flattened;
  _jacobianXj.block<9,1>(0,4) = dr_y_flattened;
  _jacobianXj.block<9,1>(0,5) = dr_z_flattened;
  _jacobianXj.block<3,3>(9,0) = Ri.transpose();
  _jacobianXj.block<3,3>(9,3) = -Ri.transpose() * skew(tj);

  _jacobianXi = -_jacobianXj;
}

} //ia namespace srrg_g2o_slim */
