#pragma once

#include "types/base_graph.h"
#include "edge_se3.h"

namespace srrg_g2o_slim {

class Graph3D : public BaseGraph {
public:

  typedef std::unordered_map<int, Isometry3> IntIsometry3Map;

  //! @brief ctor/dtor
  Graph3D();
  virtual ~Graph3D();

  virtual void clear();

  virtual void load(const std::string& filename_);

  virtual void save(const std::string& filename_) const;

  //! @brief create and add a vertex to the graph
  void addVertexSE3(const uint64_t& id_, 
                    const Isometry3& estimate_);

  //! @brief create and add an edge to the graph
  void addEdgeSE3(const int sensor_id_,
                  const uint64_t& v_from_id_,
                  const uint64_t& v_to_id_,
                  const Isometry3& measurement_,
                  const Matrix12& omega_);

  void addEdgeSE3(const int sensor_id_,
                  const uint64_t& v_from_id_,
                  const uint64_t& v_to_id_,
                  const Isometry3& measurement_,
                  const Matrix6& omega_);

  //! @brief vertexes getters
  inline const IntVertexSE3PtrMap& vertices() const {return _vertices;}
  inline IntVertexSE3PtrMap& vertices() {return _vertices;}

  //! @brief edges getters
  inline const EdgeSE3PtrVector& edges() const {return _edges;}
  inline EdgeSE3PtrVector& edges() {return _edges;}

  //! @brief offsets getters
  inline const IntIsometry3Map& sensorOffsets() const {return _sensors_offsets;}
  inline IntIsometry3Map& sensorOffsets() {return _sensors_offsets;}

  //! @brief omega threshold getter/setter
  inline const double& omegaThreshold() const {return _omega_threshold;}
  inline void setOmegaThreshold(const double& omega_threshold_) {_omega_threshold = omega_threshold_;}

protected:
  //! @brief containers
  IntVertexSE3PtrMap  _vertices; //<ID, vertex> map
  EdgeSE3PtrVector    _edges;

  //! @brief contains all the offsets of the sensors, 
  //! ordered by sensor_id
  IntIsometry3Map     _sensors_offsets;

  //! @brief threshold for omega reconditioning
  double _omega_threshold;

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

} //ia namespace srrg_g2o_slim */

