#pragma once

#include "types/base_vertex.h"

namespace srrg_g2o_slim {

class VertexSE3 : public BaseVertex<Isometry3> {
public:
  //! @brief performs the boxplus on the estimate
  void oplus(const Vector6& update_);

protected:
  //! @brief ctor/dtor
  VertexSE3() = delete;
  VertexSE3(const uint64_t& id_,
            const uint64_t& vertex_index_,
            const Isometry3& estimate_,
            bool fixed = false);
  virtual ~VertexSE3();

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  friend class Graph3D;
};

typedef std::map<int, VertexSE3*> IntVertexSE3PtrMap;
typedef std::vector<VertexSE3*> VertexSE3PtrVector;

} //ia namespace srrg_g2o_slim
