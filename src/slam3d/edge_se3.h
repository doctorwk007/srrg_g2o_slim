#pragma once

#include "types/base_edge.h"
#include "vertex_se3.h"

namespace srrg_g2o_slim {

class EdgeSE3 : public BaseEdge<Isometry3,12,Isometry3,Isometry3> {

public:
  //! @brief computes the error related to such edge
  void computeError();

  //! @brief computes the Jacobians deriving from the linearization of such edge
  void computeJacobians();

  //! @brief Jacobian_Xi getters
  inline const Matrix12_6& jacobianXi() const {return _jacobianXi;}
  inline Matrix12_6& jacobianXi() {return _jacobianXi;}

  //! @brief Jacobian_Xj getters
  inline const Matrix12_6& jacobianXj() const {return _jacobianXj;}
  inline Matrix12_6& jacobianXj() {return _jacobianXj;}

  //! @brief error getters
  inline const Vector12& error() const {return _error;}
  inline Vector12& error() {return _error;}

protected:
  //! @brief ctor/dtor
  EdgeSE3() = delete;
  EdgeSE3(VertexSE3* vertex_from_,
          VertexSE3* vertex_to_,
          const Isometry3& measurement_,
          const Matrix12& information_,
          const uint64_t& sensor_ID_);
  virtual ~EdgeSE3();

  //! jacobians
  Matrix12_6 _jacobianXi;
  Matrix12_6 _jacobianXj;
  Vector12 _error;

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  friend class Graph3D;
};

typedef std::map<int, EdgeSE3*> IntEdgeSE3PtrMap;
typedef std::vector<EdgeSE3*> EdgeSE3PtrVector;

} //ia namespace srrg_g2o_slim

