#pragma once

#include "utilities/definitions.h"

namespace srrg_g2o_slim {

template <class EstimateType>
class BaseVertex {

public:

  enum VertexMinimalDim {
    VERTEX_SE3 = 6,
    VERTEX_R3 = 3};

  //! @brief returns a deep copy of the vertex
  BaseVertex<EstimateType>* clone() const;

  //! @brief returns Vertex uint64_t
  inline const uint64_t& id() const {return _id;}

  //! @brief returns Vertex index
  inline const uint64_t& index() const {return _index;}

  //! @brief returns Vertex dimesion
  inline const VertexMinimalDim& dimension() const {return _dimension;}

  //! @brief get fixed flag of the Vertex
  inline const bool fixed() const {return _fixed;}

  //! @brief set fixed flag of the Vertex
  inline void setFixed(const bool& fixed_) {
    _fixed = fixed_;
  }

  //! @brief inline set/get
  inline void setEstimate(const Isometry3& estimate_) {
    _estimate = estimate_;
  }

  inline const Isometry3& estimate() const {
    return _estimate;
  }

protected:
  //ia cannot declare an empty Vertex
  BaseVertex() = delete;

  //! @brief default BaseVertex ctor
  //! @param[in] id_ identifier of the vertex
  //! @param[in] vertex_index_ index of the vertex in the vertex container
  //! @param[in] vertex_dimension_ minimal dimension of the current vertex (whether pose or point)
  //! @param[in] fixed_ flags that tells if the vertex must remain fixed
  BaseVertex(const uint64_t& id_,
             const uint64_t& vertex_index_,
             const VertexMinimalDim& vertex_dimension_,
             const EstimateType& estimate_,
             const bool fixed_ = false);

  //! @brief virtual dtor
  virtual ~BaseVertex();

  //ia attributes of the BaseVertex - self explenatory
  EstimateType    _estimate;
  bool            _fixed;

  const uint64_t _id;
  const uint64_t _index;

  const VertexMinimalDim _dimension;

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  friend class BaseGraph;
};

} //ia end of the namespace

#include "base_vertex.hpp"