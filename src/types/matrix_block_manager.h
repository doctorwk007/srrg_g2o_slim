#pragma once

#include "block_manager.h"

namespace srrg_g2o_slim {

  template <class MatrixBlockType>
  class MatrixBlockManager : public BlockManager {
  public:

    //ia useful typedef
    typedef MatrixBlockType Block;
    typedef std::unordered_map<IntPair, Block*, IntPairHasher, IntPairEquals > IntPairBlockPtrMap;

    //ia ctor
    MatrixBlockManager();

    //! @brief deep copy-ctor
    MatrixBlockManager(const MatrixBlockManager<MatrixBlockType>& other_);

    //! @brief ctor from a vector of indexes 
    //! @param[in] indexes_vector_: vector containing the indexes of the blocks 
    MatrixBlockManager(const std::vector<IntPair>& indexes_vector_);

    //ia dtor - deletes the blocks and frees the memory
    virtual ~MatrixBlockManager();

    //! @brief deletes the blocks and frees the memory
    void reset();

    //! @brief sets all blocks to zero
    void setZero();

    //! @brief allocates one block and sets it to identity
    //! @param[in] r_: row index of the block to allocate
    //! @param[in] c_: col index of the block to allocate
    //! @brief returns the pointer to the created block
    MatrixBlockType* allocateOneBlock(const int r_,
                                      const int c_);

    //! @brief const getter of block at (r_, c_)
    inline const Block& at(const int r_,
                           const int c_) const {
      return *_blocks_map.at(IntPair(r_, c_));
    }

    //! @brief non-const getter of block at (r_, c_)
    inline Block& at(const int r_,
                     const int c_) {
      return *_blocks_map.at(IntPair(r_, c_));
    }

    //! @brief getter of block pointer at (r_, c_)
    inline Block* getBlockPtr(const int r_,
                              const int c_) {
      return _blocks_map.at(IntPair(r_, c_));
    }

    //! @brief return the whole pull of blocks
    inline const IntPairBlockPtrMap& getBlocksMap() const {return _blocks_map;}
    inline IntPairBlockPtrMap& getBlocksMap() {return _blocks_map;}

  protected:

    //ia blocks pull
    IntPairBlockPtrMap _blocks_map;

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  };

} //ia end of the namespace

#include "matrix_block_manager.hpp"
