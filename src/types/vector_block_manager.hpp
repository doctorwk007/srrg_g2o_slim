namespace srrg_g2o_slim {

  template <class VectorBlockType>
  VectorBlockManager<VectorBlockType>::VectorBlockManager() : BlockManager() {
    //! default ctor
  }

  template <class VectorBlockType>
  VectorBlockManager<VectorBlockType>::VectorBlockManager(const VectorBlockManager<VectorBlockType>& other_) {
    for (uint64_t i = 0; i < other_._dimension; ++i) {
      Block* block = new Block();
      *block = (*other_._blocks_map.at(i));
      _blocks_map.insert(std::make_pair(i, block));
    }

    _dimension = _blocks_map.size();
    if (_dimension != other_._dimension) {
      throw std::runtime_error("Something went wrong during the construction of this VectorBlockManager, exiting.");
    }    
  }


  template <class VectorBlockType> 
  VectorBlockManager<VectorBlockType>::VectorBlockManager(const uint64_t& dimension_) {
    for (uint64_t i = 0; i < dimension_; ++i) {
      if (_blocks_map.count(i)) {
        throw std::runtime_error("Something went wrong during the construction of this VectorBlockManager, exiting.");
      } else {
        Block* block = new Block();
        block->setZero();
        _blocks_map.insert(std::make_pair(i, block));
      }
    }
  }


  template <class VectorBlockType>
  VectorBlockManager<VectorBlockType>::~VectorBlockManager() {
    typename IntVectorBlockPtrMap::iterator block_it = _blocks_map.begin();
    while (block_it != _blocks_map.end()) {
      delete block_it->second;
      ++block_it;
    }

    _blocks_map.clear();
  }


  template <class VectorBlockType>
  void VectorBlockManager<VectorBlockType>::reset() {
    typename IntVectorBlockPtrMap::iterator block_it = _blocks_map.begin();
    while (block_it != _blocks_map.end()) {
      delete block_it->second;
      ++block_it;
    }

    _blocks_map.clear();
    _dimension = 0;
  }



  template <class VectorBlockType>
  void VectorBlockManager<VectorBlockType>::setZero() {
    typename IntVectorBlockPtrMap::iterator block_it = _blocks_map.begin();
    while (block_it != _blocks_map.end()) {
      block_it->second->setZero();
      ++block_it;
    }
  }


  template <class VectorBlockType>
  void VectorBlockManager<VectorBlockType>::allocate(const uint64_t& number_of_blocks_) {
    for (uint64_t i = 0; i < number_of_blocks_; ++i) {
      if (_blocks_map.count(i)) {
        throw std::runtime_error("Something went wrong during the allocation of the blocks, exiting.");
      } else {
        Block* block = new Block();
        block->setZero();
        _blocks_map.insert(std::make_pair(i, block));
      }
    }
  }


  template <class VectorBlockType>
  VectorBlockType* VectorBlockManager<VectorBlockType>::allocateOneBlock(const uint64_t& index_) {
    //ia if the block already exists, set it to zero
    if (_blocks_map.count(index_)) {
      _blocks_map[index_]->setZero();
    } else {
      Block* block = new Block();
      block->setZero();
      _blocks_map.insert(std::make_pair(index_, block));
      ++_dimension;
    }
    return _blocks_map[index_];
  }

}//ia end of the namespace
