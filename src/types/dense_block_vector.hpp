namespace srrg_g2o_slim {

template <class BlockType>
DenseBlockVector<BlockType>::DenseBlockVector() {
  _size = 0;
  _has_storage = true;
  _is_initialized = false;
}

template <class BlockType>
DenseBlockVector<BlockType>::DenseBlockVector(const DenseBlockVector<BlockType>& other_,
                                              const bool deep_copy_flag_) {
  if (!other_._is_initialized) {
    throw std::runtime_error("initialization failed, other vector is not initialized");
  }

  _size = 0;
  _has_storage = deep_copy_flag_;

  for (uint64_t i = 0; i < other_._size; ++i) {
    if (deep_copy_flag_) {
      BlockType* new_block = _block_manager.allocateOneBlock(i);
      *new_block = other_.at(i);
      setBlockPtr(i,new_block);
    } else {
      setBlockPtr(i,other_._block_view.at(i));
    }
  }

  if (_size != other_._size)
    throw std::runtime_error("dimension mismatch, something went wrong during allocation");

  _is_initialized = true;
}

template <class BlockType>
DenseBlockVector<BlockType>::DenseBlockVector(const VectorBlockManager<BlockType>& block_manager_) {
  _size = 0;
  _has_storage = false;

  const typename VectorBlockManager<BlockType>::IntVectorBlockPtrMap& blocks_pull = block_manager_.getBlocksMap();
  typename VectorBlockManager<BlockType>::IntVectorBlockPtrMap::const_iterator block_it = blocks_pull.begin();
  while (block_it != blocks_pull.end()) {
    setBlockPtr(block_it->first, block_it->second);
    ++block_it;
  }

  if (_size != block_manager_.dimension())
    throw std::runtime_error("dimension mismatch, something went wrong during allocation");

  _is_initialized = true;
}

template <class BlockType>
DenseBlockVector<BlockType>::DenseBlockVector(const int number_of_blocks_) {
  _size = 0;
  _has_storage = true;

  for (uint64_t i = 0; i < number_of_blocks_; ++i) {
    BlockType* new_block = _block_manager.allocateOneBlock(i);
    setBlockPtr(i,new_block);
  }

  if (_size != number_of_blocks_)
    throw std::runtime_error("dimension mismatch, something went wrong during allocation");

  _is_initialized = true;
}

template <class BlockType>
DenseBlockVector<BlockType>::~DenseBlockVector() {
  if (_has_storage) {
    _block_manager.reset(); //ia frees the memory
  }
  _block_view.clear();
}

template <class BlockType>
void DenseBlockVector<BlockType>::reset() {
  if (_has_storage) {
    _block_manager.reset(); //ia frees the memory
  }
  _block_view.clear();
  _size = 0;
  _has_storage = false;
  _is_initialized = false;
}


template <class BlockType>
void DenseBlockVector<BlockType>::setZero() {
  for (uint64_t i = 0; i < _size; ++i) {
    _block_view[i]->setZero(); //ia unguarded access
  }
}

template <class BlockType>
void DenseBlockVector<BlockType>::setRandom() {
  for (uint64_t i = 0; i < _size; ++i) {
    _block_view[i]->setRandom(); //ia unguarded access
  }
}

template <class BlockType>
void DenseBlockVector<BlockType>::save(const std::string& filename_) const {
  std::ofstream file(filename_);
  for (uint64_t i = 0; i < _size; ++i) {
    const BlockType& block = (*_block_view.at(i)); 
    file << block << std::endl;
  }
  file.close();
}

template <class BlockType>
void DenseBlockVector<BlockType>::print() const {
  std::cerr << "print DenseBlockVector" << std::endl;
  for (uint64_t i = 0; i < _size; ++i) {
    printBlock(i);
  }
  std::cerr << std::endl;
}

template <class BlockType>
void DenseBlockVector<BlockType>::printBlock(const int idx_) const {
  if (idx_ >= _size) {
    throw std::runtime_error("index out of bound");
  }
  std::cerr << "block[" << idx_ << "]" << std::endl;
  std::cerr << *_block_view.at(idx_) << std::endl; 
}

template <class BlockType>
const BlockType& DenseBlockVector<BlockType>::at(const int idx_) const {
  if (idx_ >= _size) {
    throw std::runtime_error("index out of bound");
  }
  return (*_block_view.at(idx_));
}

template <class BlockType>
BlockType& DenseBlockVector<BlockType>::at(const int idx_) {
  if (idx_ >= _size) {
    throw std::runtime_error("index out of bound");
  }

  return (*_block_view[idx_]); //ia unguarded access
}

template <class BlockType>
void DenseBlockVector<BlockType>::setBlockPtr(const int idx_,
                                              BlockType* block_ptr_) {
  typename IntVectorBlockPtrMap::iterator block_it = _block_view.find(idx_);
  if (block_it == _block_view.end()) {
    _block_view.insert(std::make_pair(idx_, block_ptr_));
    ++_size;
  } else {
    block_it->second = block_ptr_;
  }
}

template <class BlockType>
BlockType* DenseBlockVector<BlockType>::getBlockPtr(const int idx_) {
  if (idx_ >= _size) {
    throw std::runtime_error("index out of bound");
  } 

  typename IntVectorBlockPtrMap::const_iterator block_it = _block_view.find(idx_);
  if (block_it == _block_view.end()) {
    return nullptr;
  }
  return block_it->second;
}

template <class BlockType>
BlockType* DenseBlockVector<BlockType>::createBlock(const int idx_) {
  if (!_has_storage) {
    throw std::runtime_error("vector does not own the blocks");
  }

  if (idx_ < _size) {
    throw std::runtime_error("block is already here");
  }

  BlockType* new_block = _block_manager.allocateOneBlock(idx_);
  setBlockPtr(idx_,new_block);

  if (!_is_initialized) {
    _is_initialized = true;
  }

  return new_block;
}

template <class BlockType>
void DenseBlockVector<BlockType>::applyPermutation(const IntVector& permutation_vector_) {
  IntVectorBlockPtrMap aux_map = _block_view;
  for (uint64_t index = 0; index < _size; ++index) {
    _block_view[permutation_vector_[index]] = aux_map[index];
  }
}

template <class BlockType>
void DenseBlockVector<BlockType>::applyInversePermutation(const IntVector& permutation_vector_) {
  IntVector inverse_permutation(permutation_vector_.size());
  for (uint64_t index = 0; index < permutation_vector_.size(); ++index) {
    inverse_permutation[permutation_vector_[index]] = index;
  }

  IntVectorBlockPtrMap aux_map = _block_view;
  for (uint64_t index = 0; index < _size; ++index) {
    _block_view[inverse_permutation[index]] = aux_map[index];
  }
}

template <class BlockType>
void DenseBlockVector<BlockType>::removeBlock(const int idx_) {
  if (idx_ >= _size) {
    throw std::runtime_error("index out of bound");
  }

  IntVectorBlockPtrMap src_view = _block_view;
  _block_view.clear();

  int new_index = 0;
  typename IntVectorBlockPtrMap::const_iterator src_block_it = src_view.begin();
  typename IntVectorBlockPtrMap::const_iterator src_block_end = src_view.end();
  while (src_block_it != src_block_end) {
    const int src_index = src_block_it->first;

    if (src_index == idx_) {
      --_size;
      ++src_block_it;
      continue;
    }

    if (src_index < idx_) {
      new_index = src_index;
    } else {
      new_index = src_index - 1;
    }

    _block_view.insert(std::make_pair(new_index, src_block_it->second));
    ++src_block_it;
  }
}

}//ia end of the namespace
