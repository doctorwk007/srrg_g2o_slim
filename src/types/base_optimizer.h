#pragma once

#include "base_graph.h"
#include "linear_solver.h"

namespace srrg_g2o_slim {

template <class MatrixType, class VectorType>
class BaseOptimizer {
public:
  BaseOptimizer();
  virtual ~BaseOptimizer();

  //! @brief destroys everything in order to be re-initialized
  //! with a new graph.
  virtual void clear() = 0;

  //! @brief allocate data structures and sets everithing up
  virtual void init() = 0;

  //! @brief performs all the iterations and updates the graph
  virtual void optimize() = 0;

  //! @brief inline set
  inline void setGraphPtr(BaseGraph* graph_ptr_) {
    _graph_ptr = graph_ptr_;
  }

  //! @brief inline set
  inline void setLinearSolverPtr(LinearSolver<MatrixType,VectorType>* solver_ptr_) {
    _solver_ptr = solver_ptr_;
  }

  //! @brief inline set/get
  inline void setIterations(const uint64_t& iterations_) {
    _iterations = iterations_;
  }

  inline const uint64_t& iterations() const {
    return _iterations;
  }

  //! @brief inline set/get
  inline void setKernelWidth(const Real& kernel_width_) {
    _kernel_width = kernel_width_;
  }

  inline const Real& kernelWidth() const {
    return _kernel_width;
  }

  //! @brief inline get
  inline const Real& totalChi() const {return _total_chi;}

  //! @brief inline set/get
  inline void setVerbose(const bool verbose_) {
    _verbose = verbose_;
  }

  inline const bool verbose() const {
    return _verbose;
  }

protected:
  //! @brief performs one step of optimization
  virtual void _oneStep() = 0;

  //! @brief applies the update computed during the optimization
  //! step to the graph.
  virtual void _applyUpdate() = 0;

  //! @brief ptr to the graph to optimize (not owned by the optimizer)
  BaseGraph* _graph_ptr = nullptr;

  //! @brief ptr to the linear solver (not owned by the optimizer)
  LinearSolver<MatrixType, VectorType>* _solver_ptr = nullptr;

  //! @brief number of iterations
  uint64_t _iterations;

  //! @brief (retarded) kernel width
  Real _kernel_width;

  //! @brief total chi
  Real _total_chi;

  //! @brief inliers and outliers
  uint64_t _inliers;
  uint64_t _outliers;

  //! @brief verbose flag
  bool _verbose;

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

} //ia namespace srrg_g2o_slim

#include "base_optimizer.hpp"

