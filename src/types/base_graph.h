#pragma once

#include "base_edge.h"

namespace srrg_g2o_slim {

class BaseGraph {
public:
  //ia ctor
  BaseGraph();

  //ia dtor
  virtual ~BaseGraph();
  
  //! @brief clears the graph, deleting vertices and edges
  virtual void clear() = 0;

  //! @brief load a graph from .g2o file 
  virtual void load(const std::string& filename_) = 0;

  //! @brief save the graph in a .g2o file 
  virtual void save(const std::string& filename_) const = 0;

  //! @brief get the number of vertices
  inline const uint64_t& numVertices() const {return _num_vertices;}

  //! @brief get the number of edges
  inline const uint64_t& numEdges() const {return _num_edges;}

  //! @brief get the number of fixed vertices
  inline const uint64_t& numFixed() const {return _num_fixed_vertices;}

protected:

  //ia counters
  uint64_t _num_vertices;
  uint64_t _num_edges;
  uint64_t _num_fixed_vertices;

  //ia counter to create an index in the graph.
  static uint64_t     _vertex_index;

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

};

} //ia end of the namespace
