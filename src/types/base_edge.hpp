namespace srrg_g2o_slim {

template <class MeasurementType, int InformationSize, class FromType, class ToType>
BaseEdge<MeasurementType, InformationSize, FromType, ToType>::BaseEdge(BaseVertex<FromType>* vertex_from_,
                                                         BaseVertex<ToType>* vertex_to_,
                                                         const MeasurementType& measurement_,
                                                         const MatrixD& information_,
                                                         const int& sensor_id_,
                                                         const EdgeType& edge_type_) :
                                                               _sensor_id(sensor_id_),
                                                               _type(edge_type_),
                                                               _vertex_association(vertex_from_, vertex_to_) {
  _measurement = measurement_;
  _information = information_;
}


template <class MeasurementType, int InformationSize, class FromType, class ToType>
BaseEdge<MeasurementType, InformationSize, FromType, ToType>::~BaseEdge() {}


template <class MeasurementType, int InformationSize, class FromType, class ToType>
BaseEdge<MeasurementType, InformationSize, FromType, ToType>* BaseEdge<MeasurementType, InformationSize, FromType, ToType>::clone() const {
  BaseEdge<MeasurementType, InformationSize, FromType, ToType>* cloned_edge =
      new BaseEdge<MeasurementType, InformationSize, FromType, ToType>(_vertex_association.from_ptr,
          _vertex_association.to_ptr, _measurement,
          _information, _sensor_id, _type);
  return cloned_edge;
}


}//ia end of the namespace
