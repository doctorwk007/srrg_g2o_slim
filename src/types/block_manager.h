#pragma once

#include "utilities/definitions.h"

namespace srrg_g2o_slim {

  class BlockManager {
  public:
    //ia ctor/dtor
    BlockManager();
    BlockManager(const uint64_t& number_of_blocks_);
    virtual ~BlockManager();
    
    //! @brief destroys the memory allocated
    virtual void reset() = 0;

    //! @brief sets to zero all the blocks
    virtual void setZero() = 0;

    //! @brief dimension getter
    inline const uint64_t& dimension() const {return _dimension;}

  protected:
    //! @brief number of blocks
    uint64_t _dimension;
  };
  
}//ia end of the namespace
