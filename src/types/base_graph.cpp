#include "base_graph.h"

namespace srrg_g2o_slim {

uint64_t BaseGraph::_vertex_index = 0;
  
BaseGraph::BaseGraph() {
  _num_vertices       = 0;
  _num_edges          = 0;
  _num_fixed_vertices = 0;
}

BaseGraph::~BaseGraph() {}

}//ia end of the namespace
