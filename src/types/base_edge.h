#pragma once

#include "base_vertex.h"

namespace srrg_g2o_slim {

//! TODO better define the parameters of the template
template <class MeasurementType, int InformationDim, class FromType, class ToType>
class BaseEdge {

public:

  enum EdgeType {
    EDGE_SE3,
    EDGE_SE3_R3};

  struct VertexPtrAssociation {
    VertexPtrAssociation(BaseVertex<FromType>* from_, BaseVertex<ToType>* to_) {
      from_ptr = from_;
      to_ptr = to_;
    }

    BaseVertex<FromType>* from_ptr;
    BaseVertex<ToType>* to_ptr;
  };

  typedef typename Eigen::Matrix<Real, InformationDim, InformationDim> MatrixD;

  //! @brief returns a deep copy of the edge
  virtual BaseEdge<MeasurementType, InformationDim, FromType, ToType>* clone() const;

  //! @brief measurement getter
  inline const MeasurementType& measurement() const {return _measurement;}
  inline MeasurementType& measurement() {return _measurement;}

  //! @brief information matrix getter
  inline const MatrixD& information() const {return _information;}
  inline MatrixD& information() {return _information;}

  //! @brief vertex_association getter
  inline const VertexPtrAssociation& vertexAssociation() const {return _vertex_association;}

  //! @brief sensor_id getter
  inline const int& sensorID() const {return _sensor_id;}

  //! @brief edge type getter
  inline const EdgeType& type() const {return _type;}

protected:
  //ia cannot declare an empty edge
  BaseEdge() = delete;

  //! @brief default BaseEdge (A->B) ctor
  //! @param[in] vertex_from_: pointer to the vertex A
  //! @param[in] vertex_to_: pointer to the vertex B
  //! @param[in] measurement_: measurement from A to B
  //! @param[in] measurement_: information matrix of the measurement
  //! @param[in] sensor_id_: id of the sensor that is creating this edge
  //! @param[in] edge_type_: decribes the nature of the edge (SE3, SE3->R3, ..)
  BaseEdge(BaseVertex<FromType>* vertex_from_,
           BaseVertex<ToType>* vertex_to,
           const MeasurementType& measurement_,
           const MatrixD& information_,
           const int& sensor_id_,
           const EdgeType& edge_type_);

  //! @brief virtual dtor
  virtual ~BaseEdge();

  //ia attributes of the BaseEdge - self explanatory
  MeasurementType _measurement;
  MatrixD _information;


  const int _sensor_id;
  const EdgeType _type;
  const VertexPtrAssociation _vertex_association;  //ia pointers to the vertices involved

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  friend class BaseGraph;
};


}//ia end of the namespace

#include "base_edge.hpp"