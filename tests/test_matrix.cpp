#include "types/sparse_block_matrix.h"

using namespace srrg_g2o_slim;
typedef Matrix2 Block;

int main(int argc, char **argv) {
  std::cerr << "matrix 1\n" << std::endl;

  SparseBlockMatrix<Block>* m1 = new SparseBlockMatrix<Matrix2>(3,3);
  std::cerr << "nnz: " << m1->nnz() << std::endl;
  m1->print();

  for (int row_idx = 0; row_idx < m1->rows(); ++row_idx) {
    for (int col_idx = 0; col_idx < m1->cols(); ++col_idx) {
      Block* b1 = m1->createBlock(row_idx,col_idx);
    }
  }

  std::cerr << "nnz            : " << m1->nnz() << std::endl;
  std::cerr << "has_storage    : " << m1->hasStorage() << std::endl;
  std::cerr << "is_initialized : " << m1->isInitialized() << std::endl;
  m1->print();

  m1->setRandom();
  m1->print();

  m1->setIdentity();
  m1->print();

  m1->setZero();
  m1->print();

  Block b;
  b << 1,2,3,4;
  m1->at(0,0) = b;
  m1->at(2,2) = b.transpose();
  m1->print();

  m1->savePattern("test_matrix_pattern.txt");

  m1->reset();
  std::cerr << "nnz            : " << m1->nnz() << std::endl;
  std::cerr << "has_storage    : " << m1->hasStorage() << std::endl;
  std::cerr << "is_initialized : " << m1->isInitialized() << std::endl;
  m1->print();

  std::cerr << "\n\n*****************************" << std::endl;
  std::cerr << "matrix 2\n" << std::endl;

  MatrixBlockManager<Block> mman;
  mman.allocateOneBlock(0,0);
  mman.allocateOneBlock(1,1);
  mman.allocateOneBlock(2,2);
  std::cerr << "manager dimension: " << mman.dimension() << std::endl;

  SparseBlockMatrix<Block>* m2 = new SparseBlockMatrix<Block>(3,3,mman);
  m2->print();
  std::cerr << "nnz            : " << m2->nnz() << std::endl;
  std::cerr << "has_storage    : " << m2->hasStorage() << std::endl;
  std::cerr << "is_initialized : " << m2->isInitialized() << std::endl;

  mman.at(0,0).setRandom();
  mman.at(1,1).setRandom();
  mman.at(2,2).setRandom();
  m2->print();

  std::cerr << "\n\n*****************************" << std::endl;
  std::cerr << "matrix 3, light copy of matrix 2\n" << std::endl;
  std::cerr << "light copy" << std::endl;

  SparseBlockMatrix<Block>* m3 = new SparseBlockMatrix<Block>(*m2);
  std::cerr << "nnz            : " << m3->nnz() << std::endl;
  std::cerr << "has_storage    : " << m3->hasStorage() << std::endl;
  std::cerr << "is_initialized : " << m3->isInitialized() << std::endl;
  m3->print();

  std::cerr << "\n\n*****************************" << std::endl;
  std::cerr << "matrix 4, deep copy of matrix 2\n" << std::endl;
  std::cerr << "deep copy" << std::endl;

  SparseBlockMatrix<Block>* m4 = new SparseBlockMatrix<Block>(*m2, true);
  std::cerr << "nnz            : " << m4->nnz() << std::endl;
  std::cerr << "has_storage    : " << m4->hasStorage() << std::endl;
  std::cerr << "is_initialized : " << m4->isInitialized() << std::endl;
  m4->print();

  std::cerr << "\n\n*****************************" << std::endl;
  std::cerr << "matrix 2, 3 and 4 after modify mman\n" << std::endl;
  mman.setZero();
  m2->print();
  m3->print();
  m4->print();
/**/
  std::cerr << "\n\n*****************************" << std::endl;
  std::cerr << "matrix5\n" << std::endl;

  MatrixBlockManager<Block> mman2;
  mman2.allocateOneBlock(0,0);
  mman2.allocateOneBlock(1,1);
  mman2.allocateOneBlock(1,0);
  mman2.allocateOneBlock(2,2);
  mman2.allocateOneBlock(2,0);
  mman2.allocateOneBlock(2,1);
  mman2.allocateOneBlock(3,3);
  mman2.allocateOneBlock(3,2);
  mman2.allocateOneBlock(4,4);
  mman2.allocateOneBlock(4,3);

  SparseBlockMatrix<Block>* m5 = new SparseBlockMatrix<Block>(5,5,mman2);
  m5->at(1,0) = Block::Ones()*2;
  m5->at(2,0) = Block::Ones()*5;
  m5->at(2,1) = Block::Ones()*10;
  m5->at(3,2) = Block::Ones()*20;
  m5->at(4,3) = Block::Ones()*40;

  std::cerr << "nnz            : " << m5->nnz() << std::endl;
  std::cerr << "has_storage    : " << m5->hasStorage() << std::endl;
  std::cerr << "is_initialized : " << m5->isInitialized() << std::endl;
  m5->print();

//  int row_to_remove = 2;
//  std::cerr << "removing row " << row_to_remove << std::endl << std::endl;;
//  m5->removeRow(row_to_remove);
//
//  std::cerr << "nnz            : " << m5->nnz() << std::endl;
//  std::cerr << "has_storage    : " << m5->hasStorage() << std::endl;
//  std::cerr << "is_initialized : " << m5->isInitialized() << std::endl;
//  m5->print();
//
//  int col_to_remove = 2;
//  std::cerr << "removing col " << col_to_remove << std::endl << std::endl;;
//  m5->removeCol(col_to_remove);

  std::cerr << "removing index" << 2 << std::endl << std::endl;;
  m5->removeRowAndCol(2);

  std::cerr << "nnz            : " << m5->nnz() << std::endl;
  std::cerr << "has_storage    : " << m5->hasStorage() << std::endl;
  std::cerr << "is_initialized : " << m5->isInitialized() << std::endl;
  m5->print();

  std::cerr << "removing index" << 0 << std::endl << std::endl;;
  m5->removeRowAndCol(0);

  std::cerr << "nnz            : " << m5->nnz() << std::endl;
  std::cerr << "has_storage    : " << m5->hasStorage() << std::endl;
  std::cerr << "is_initialized : " << m5->isInitialized() << std::endl;
  m5->print();


  std::cerr << "\n\n*****************************" << std::endl;
  std::cerr << "cholesky test :O\n" << std::endl;

  Eigen::MatrixXd octave_matrix(10,10);
  octave_matrix <<
      3.1554,2.4834,2.1498,2.3529,1.6693,1.4177,2.5764,3.0631,2.5105,1.6553,
      2.4834,3.748,1.6821,2.4051,1.9402,1.9816,2.3929,2.8495,2.7558,2.5109,
      2.1498,1.6821,2.6447,1.9213,1.4884,1.33,2.0353,2.9412,2.3814,1.3809,
      2.3529,2.4051,1.9213,3.3287,1.9326,2.5068,2.0494,3.3702,1.9715,2.079,
      1.6693,1.9402,1.4884,1.9326,1.8247,1.5089,1.1823,1.9442,1.6581,1.4534,
      1.4177,1.9816,1.33,2.5068,1.5089,2.2361,1.4354,2.6134,1.5537,1.9458,
      2.5764,2.3929,2.0353,2.0494,1.1823,1.4354,2.9966,3.4455,2.5577,1.5068,
      3.0631,2.8495,2.9412,3.3702,1.9442,2.6134,3.4455,4.8432,3.236,2.3077,
      2.5105,2.7558,2.3814,1.9715,1.6581,1.5537,2.5577,3.236,3.0489,1.9627,
      1.6553,2.5109,1.3809,2.079,1.4534,1.9458,1.5068,2.3077,1.9627,2.332;

  SparseBlockMatrix<Matrix2>* m6 = new SparseBlockMatrix<Matrix2>(5,5);
  for (int r = 0; r < 5; ++r) {
    for (int c = 0; c < 5; ++c) {
      Matrix2* block = m6->createBlock(r,c);
      *block = octave_matrix.block<2,2>(r*2,c*2);
    }
  }

  SparseBlockMatrix<Matrix2>* ch = nullptr;
  m6->allocateCholesky(ch);
  m6->computeCholesky(ch);

  ch->print();

  std::cerr << "\n\n*****************************" << std::endl;
  std::cerr << "permutation test :/\n" << std::endl;
  SparseBlockMatrix<Block>* m7 = new SparseBlockMatrix<Block>(5,5);
  IntVector p(m7->rows());
  int k = 0;
  int p_entry = m7->rows() - 1;
  for (int r = 0; r < m7->rows(); ++r) {
    for (int c = 0; c < m7->cols(); ++c) {
      Block* block = m7->createBlock(r,c);
      *block = Block::Ones() * k;
      ++k;
    }
    p[r] = p_entry;
    --p_entry;
  }

  m7->print();

  std::cerr << "apply permutation" << std::endl;
  m7->applyPermutation(p);
  m7->print();

  std::cerr << "apply inverse permutation" << std::endl;
  m7->applyInversePermutation(p);
  m7->print();

  std::cerr << "\n\n*****************************" << std::endl;
  std::cerr << "load test :/\n" << std::endl;
  std::cerr << "\n\n*****************************" << std::endl;
  std::cerr << "load:/\n" << std::endl;

  SparseBlockMatrix<Matrix2>* m8 = new SparseBlockMatrix<Matrix2>(5,5);
  m8->load("/home/istin/Desktop/serialized_block_matrix.txt");
  m8->print();

  std::cerr << "\n\n*****************************" << std::endl;
  std::cerr << "multiple deletions :/\n" << std::endl;

  SparseBlockMatrix<Matrix2>* m9 = new SparseBlockMatrix<Matrix2>(6,6);
  int porcodio = 1;
  for (int i = 0; i < m9->rows(); ++i) {
    for (int j = 0; j < m9->cols(); ++j) {
      Matrix2* block = m9->createBlock(i,j);
      *block = Matrix2::Ones() * porcodio++;
    }
  }

  m9->print();

  std::set<int> removed_indexes;
  std::vector<int> real_removed_indexes;
  removed_indexes.insert(1);
  removed_indexes.insert(4);
  

  std::set<int>::const_iterator it = removed_indexes.begin();
  for ( ; it != removed_indexes.end(); ++it) {
    std::cerr << GREEN << *it << std::endl << RESET;
  }

  int decrementer = 0;
  it = removed_indexes.begin();
  for ( ; it != removed_indexes.end(); ++it) {
    int new_index = *it-decrementer++;
    if(new_index < 0)
      throw std::runtime_error("negative index");
    real_removed_indexes.push_back(new_index);
  }

  for (int j = 0; j < real_removed_indexes.size(); ++j) {
    std::cerr << real_removed_indexes[j] << std::endl;
    m9->removeRowAndCol(real_removed_indexes[j]);
  }

  m9->print();


  delete m1;
  delete m2;
  delete m3;
  delete m4;
  delete m5;
  delete m6;
  delete ch;
  delete m7;
  delete m8;
  delete m9;
  return 0;
}



