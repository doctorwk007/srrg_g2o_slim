#include "slam3d/optimizer_3d.h"
#include "slam3d/graph_3d.h"
#include "utilities/system_utils.h"

using namespace srrg_g2o_slim;

int main(int argc, char **argv) {

  typedef Matrix6 MatrixBlock;
  typedef Vector6 VectorBlock;

  if (argc < 3 || argc > 3) {
    std::cerr << "usage: " << argv[0] << " <input.g2o> <output.g2o>" << std::endl;
    exit(EXIT_FAILURE);
  }

  std::string in_filename(argv[1]);
  std::string out_filename(argv[2]);
  std::cerr << "Optimizer Test"<< std::endl << std::endl;

  //ia load a graph from file;
  Graph3D* graph = new Graph3D();
  graph->load(in_filename);

  //ia declare a linear solver
  LinearSolver<MatrixBlock, VectorBlock>* solver = new LinearSolver<MatrixBlock, VectorBlock>();
  solver->setOrdering(SUITESPARSE_AMD_ORDERING);

  //ia declare an optimizer
  Optimizer3D* optimizer = new Optimizer3D();
  optimizer->setIterations(10);
  optimizer->setKernelWidth(5e7);
  optimizer->setVerbose(true);
  optimizer->setGraphPtr(graph);
  optimizer->setLinearSolverPtr(solver);

  //ia initialize optimizer
  double t0 = getTime();
  optimizer->init();
  optimizer->optimize();
  std::cerr << "TOTAL TOTAL TIME = " << getTime() - t0 << std::endl;

  graph->save(out_filename);

  delete optimizer;
  delete solver;
  delete graph;
  return 0;
}

