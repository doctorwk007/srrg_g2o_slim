#include "slam3d/optimizer_3d.h"
#include "slam3d/graph_3d.h"
#include <algorithm>    // std::random_shuffle
#include <ctime>        // std::time
#include <cstdlib>      // std::rand, std::srand
#include "utilities/system_utils.h"

// TODO check out the addresses if they are contigous DURING THE CH_CALCULATION


using namespace srrg_g2o_slim;

typedef Matrix6 MatrixBlock;
typedef Vector6 VectorBlock;

int main(int argc, char **argv) {
  std::string in_filename(argv[1]);
  std::cerr << "Optimizer Test"<< std::endl << std::endl;

  //ia load a graph from file;
  Graph3D* graph = new Graph3D();
  graph->load(in_filename);

  //ia declare a linear solver
  LinearSolver<MatrixBlock, VectorBlock>* solver = new LinearSolver<MatrixBlock, VectorBlock>();
  solver->setOrdering(PLAIN_ORDERING);

  //ia declare an optimizer
  Optimizer3D* optimizer = new Optimizer3D();
  optimizer->setIterations(1);
  optimizer->setKernelWidth(5e7);
  optimizer->setVerbose(true);
  optimizer->setGraphPtr(graph);
  optimizer->setLinearSolverPtr(solver);

  //ia initialize optimizer
  double t0 = getTime();
  optimizer->init();
  optimizer->optimize();

  delete optimizer;
  delete solver;
  delete graph;
}


