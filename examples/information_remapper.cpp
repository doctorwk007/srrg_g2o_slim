#include "slam3d/graph_3d.h"

using namespace srrg_g2o_slim;


int main(int argc, char *argv[]) {
  if (argc < 3 || argc > 3) {
    std::cerr << "this converts the edges' information matrix of the input g2o 3D graph" << std::endl;
    std::cerr << "with a scaled one" << std::endl;
    std::cerr << "usage: " << argv[0] << " <input.g2o> <output.g2o>" << std::endl;
    exit(EXIT_FAILURE);
  }

  std::string in_filename(argv[1]);
  std::string out_filename(argv[2]);

  Graph3D* g = new Graph3D();
  std::cerr << "loading . . ." << std::endl;  
  g->load(in_filename);

  bool remap_rotational = true;
  bool remap_translational = true;

  Eigen::Matrix<Real, 9, 9> scaling = Eigen::Matrix<Real, 9, 9>::Identity() * 0.01;

  EdgeSE3PtrVector& edges = g->edges();
  for (EdgeSE3* e_ptr : edges) {
    int information_dim = e_ptr->information().rows();
    Matrix12& information = e_ptr->information();
    double rate = information(0,0) / information(11,11);
    // std::cerr << "I_angle/I_trans = " << rate << std::endl;
    if (rate > 100.0) {
      information.block<9,9>(0,0) *= scaling;
    }
  }

  std::cerr << "saving . . ." << std::endl;  
  g->save(out_filename);

  delete g;
  return 0;
}