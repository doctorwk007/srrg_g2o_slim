#include "slam3d/graph_3d.h"
#include "utilities/geometry_utils.h"
#include "utilities/unscented.h"

using namespace srrg_g2o_slim;


//ia from 12D back to standard 6D
Matrix6 _remapBackInformationMatrix(const Vector12& src_mean_,
                                    const Matrix12& src_omega_);


int main(int argc, char *argv[]) {
  if (argc < 3 || argc > 3) {
    std::cerr << "this converts the edges' information matrix of the input g2o 3D graph" << std::endl;
    std::cerr << "with the one computed with the unscented transform" << std::endl;
    std::cerr << "usage: " << argv[0] << " <input.g2o> <output.g2o>" << std::endl;
    exit(EXIT_FAILURE);
  }

  std::string in_filename(argv[1]);
  std::string out_filename(argv[2]);

  Graph3D* g = new Graph3D();
  std::cerr << "loading . . ." << std::endl;  
  g->load(in_filename);

  std::cerr << "converting . . ." << std::endl;
  std::ofstream file(out_filename);

  Vector3 t;
  QuaternionReal q;
  //ia get things
  const Graph3D::IntIsometry3Map& sensor_off_map = g->sensorOffsets();
  const IntVertexSE3PtrMap& vertices = g->vertices();
  const EdgeSE3PtrVector& edges = g->edges();

  Graph3D::IntIsometry3Map::const_iterator s_it = sensor_off_map.begin();
  Graph3D::IntIsometry3Map::const_iterator s_end = sensor_off_map.end();
  while (s_it != s_end) {
    const Isometry3 invT = s_it->second.inverse();
    q = QuaternionReal(invT.linear());
    t = invT.translation();

    file << "PARAMS_SE3OFFSET" << " ";
    file << s_it->first << " ";

    file << t.x() << " " << t.y() << " " << t.z() << " ";
    file << q.x() << " " << q.y() << " " << q.z() << " " << q.w() << " ";
    file << std::endl;
    ++s_it;
  }

  file << std::endl;

  //ia now vertices
  IntVertexSE3PtrMap::const_iterator v_it = vertices.begin();
  IntVertexSE3PtrMap::const_iterator v_end = vertices.end();
  while (v_it != v_end) {
    const VertexSE3* v = v_it->second;

    if (v->dimension() != BaseVertex<Isometry3>::VertexMinimalDim::VERTEX_SE3) {
      throw std::runtime_error("unknown vertex type");
    }

    const Isometry3& T = v->estimate();
    t = T.translation();
    q = QuaternionReal(T.linear());

    file << "VERTEX_SE3:QUAT" << " ";
    file << v->id() << " ";
    file << t.x() << " " << t.y() << " " << t.z() << " ";
    file << q.x() << " " << q.y() << " " << q.z() << " " << q.w() << " " << std::endl;

    if (v->fixed()) {
      file << "FIX" << " " << v->id() << std::endl;
    }

    ++v_it;
  }


  file << "\n\n\n";

  //ia finally edges
  for (uint64_t e_idx = 0; e_idx < edges.size(); ++e_idx) {
    const EdgeSE3* e = edges[e_idx];

    file << "EDGE_SE3:QUAT" << " ";
    const int v_from_id = e->vertexAssociation().from_ptr->id();
    const int v_to_id = e->vertexAssociation().to_ptr->id();
    const Isometry3& T = e->measurement();
    const Matrix12& omega = e->information();

    t = T.translation();
    q = QuaternionReal(T.linear());

    file << v_from_id << " " << v_to_id << " ";
    file << t.x() << " " << t.y() << " " << t.z() << " ";

    file << q.x() << " " << q.y() << " " << q.z() << " " << q.w() << " ";

    //ia transform back the omega
    Matrix6 transformed_omega = Matrix6::Zero();
    Vector12 flatten_T = flattenIsometry(T);
    transformed_omega = _remapBackInformationMatrix(flatten_T, omega);

    for (int r = 0; r < transformed_omega.rows(); ++r) {
      for (int c = r; c < transformed_omega.cols(); ++c) {
        file << transformed_omega(r,c) << " ";
      }
    }

    file << std::endl;
  }

  file << "\n\n";
  file.close();

  delete g;
  return 0;
}




//ia from 12D back to standard 6D
Matrix6 _remapBackInformationMatrix(const Vector12& src_mean_,
                                    const Matrix12& src_omega_) {

  const Matrix12 src_sigma = src_omega_.inverse();

  Vector6 remapped_mean = Vector6::Zero();
  Matrix6 remapped_sigma = Matrix6::Zero();
  Matrix6 remapped_omega = Matrix6::Zero();
  
  SigmaPoint12Vector sigma_points_12D;
  SigmaPoint6Vector sigma_points_6D;

  sampleUnscented(src_mean_, src_sigma, sigma_points_12D);

  int k = 1;
  for (int i = 0; i < sigma_points_12D[0].sample.size(); ++i) {
    int sample_plus_idx = k++;
    int sample_minus_idx = k++;

    const SigmaPoint12& sigma_point12_plus = sigma_points_12D[sample_plus_idx];
    const SigmaPoint12& sigma_point12_minus = sigma_points_12D[sample_minus_idx];

    const Vector12& sample_12D_plus = sigma_point12_plus.sample;
    const Vector12& sample_12D_minus = sigma_point12_minus.sample;

    Isometry3 T_plus = unflattenIsometry(sample_12D_plus, true);
    Isometry3 T_minus = unflattenIsometry(sample_12D_minus, true);

    Vector6 sample_6D_plus = t2vMQT(T_plus);
    Vector6 sample_6D_minus = t2vMQT(T_minus);

    SigmaPoint6 point_6D_plus(sample_6D_plus, 
      sigma_point12_plus.wi, 
      sigma_point12_plus.wp);
    SigmaPoint6 point_6D_minus(sample_6D_minus, 
      sigma_point12_minus.wi, 
      sigma_point12_minus.wp);
    sigma_points_6D.push_back(point_6D_plus);
    sigma_points_6D.push_back(point_6D_minus);
  }

  reconstructGaussian(sigma_points_6D, remapped_mean, remapped_sigma);
  remapped_omega = remapped_sigma.inverse();
  return remapped_omega;
}
