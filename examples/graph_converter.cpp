#include "slam3d/graph_3d.h"

using namespace srrg_g2o_slim;

int main(int argc, char *argv[]) {
  if (argc < 4 || argc > 4) {
    std::cerr << "this converts the edges' information matrix of the input g2o 3D graph" << std::endl;
    std::cerr << "with the one computed with the unscented transform" << std::endl;
    std::cerr << "usage: " << argv[0] << " <input.g2o> <output.g2o> <omega_threshold>" << std::endl;
    exit(EXIT_FAILURE);
  }

  std::string in_filename(argv[1]);
  std::string out_filename(argv[2]);
  const double thresh(atof(argv[3]));

  Graph3D* g = new Graph3D();
  g->setOmegaThreshold(thresh);
  std::cerr << "loading . . ." << std::endl;  
  g->load(in_filename);

  std::cerr << "converting . . ." << std::endl;
  g->save(out_filename);

  delete g;
  return 0;
}
